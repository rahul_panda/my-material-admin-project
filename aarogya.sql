-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 09, 2023 at 05:24 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aarogya`
--

-- --------------------------------------------------------

--
-- Table structure for table `appointments`
--

CREATE TABLE `appointments` (
  `id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `appointment_date` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `appointments`
--

INSERT INTO `appointments` (`id`, `doctor_id`, `patient_id`, `appointment_date`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2023-05-20 09:00:00', '2023-05-19 15:25:36', '2023-05-19 15:25:36'),
(2, 2, 2, '2023-05-21 14:30:00', '2023-05-19 15:25:36', '2023-05-19 15:25:36'),
(3, 1, 3, '2023-05-22 11:15:00', '2023-05-19 15:25:36', '2023-05-19 15:25:36'),
(4, 3, 2, '2023-05-23 16:00:00', '2023-05-19 15:25:36', '2023-05-19 15:25:36');

-- --------------------------------------------------------

--
-- Table structure for table `doctors`
--

CREATE TABLE `doctors` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `specialization` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `doctors`
--

INSERT INTO `doctors` (`id`, `first_name`, `last_name`, `email`, `phone_number`, `specialization`, `created_at`, `updated_at`) VALUES
(1, 'John', 'Doe', 'johndoe@example.com', '1234567890', 'Cardiology', '2023-05-18 16:11:33', '2023-05-18 16:11:33'),
(2, 'Jane', 'Smith', 'janesmith@example.com', '9876543210', 'Pediatrics', '2023-05-18 16:11:33', '2023-05-18 16:11:33'),
(3, 'Michael', 'Johnson', 'michaeljohnson@example.com', '4567890123', 'Orthopedics', '2023-05-18 16:11:33', '2023-05-18 16:11:33'),
(4, 'Emily', 'Wilson', 'emilywilson@example.com', '7890123456', 'Dermatology', '2023-05-18 16:11:33', '2023-05-18 16:11:33'),
(5, 'Daniel', 'Brown', 'danielbrown@example.com', '5678901234', 'Gastroenterology', '2023-05-18 16:11:33', '2023-05-18 16:11:33');

-- --------------------------------------------------------

--
-- Table structure for table `patients`
--

CREATE TABLE `patients` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `address` varchar(255) NOT NULL,
  `date_of_birth` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `patients`
--

INSERT INTO `patients` (`id`, `first_name`, `last_name`, `email`, `phone_number`, `address`, `date_of_birth`, `created_at`, `updated_at`) VALUES
(1, 'John', 'Doe', 'johndoe@example.com', '9340623274', '123 Main St, City', '1989-12-22', '2023-05-16 08:57:22', '2023-08-17 11:19:34'),
(2, 'Jane', 'Smith', 'janesmith@example.com', '9876543210', '456 Elm St, City', '1992-05-11', '2023-05-17 17:57:22', '2023-05-17 17:57:22'),
(3, 'Michael', 'Johnson', 'michaeljohnson@example.com', '5555555555', '789 Oak St, City', '1985-09-20', '2023-05-18 15:57:22', '2023-05-18 15:57:22'),
(4, 'Emily', 'Davis', 'emilydavis@example.com', '1111111111', '987 Maple St, City', '1998-12-10', '2023-05-18 15:57:22', '2023-05-18 15:57:22'),
(5, 'David', 'Anderson', 'davidanderson@example.com', '2222222222', '654 Pine St, City', '1994-07-18', '2023-05-18 15:57:22', '2023-05-18 15:57:22'),
(6, 'Sophia', 'Wilson', 'sophiawilson@example.com', '3333333333', '321 Cedar St, City', '1997-03-25', '2023-05-18 15:57:22', '2023-05-18 15:57:22'),
(7, 'Oliver', 'Thomas', 'oliverthomas@example.com', '4444444444', '852 Birch St, City', '1993-11-05', '2023-05-18 15:57:22', '2023-05-18 15:57:22'),
(8, 'Emma', 'Martinez', 'emmamartinez@example.com', '5555555555', '753 Spruce St, City', '1991-08-12', '2023-05-18 15:57:22', '2023-05-18 15:57:22'),
(9, 'Ethan', 'Taylor', 'ethantaylor@example.com', '6666666666', '159 Walnut St, City', '1996-06-30', '2023-05-18 15:57:22', '2023-05-18 15:57:22'),
(10, 'Ava', 'Harris', 'avaharris@example.com', '7777777777', '357 Pine St, City', '1999-02-28', '2023-05-18 15:57:22', '2023-05-18 15:57:22'),
(11, 'Testinggg', 'example', 'mytest@example.com', '7777777888', '357 Pine St, yes City', '1999-02-27', '2023-05-18 10:27:22', '2023-05-18 10:27:22');

-- --------------------------------------------------------

--
-- Table structure for table `prescriptions`
--

CREATE TABLE `prescriptions` (
  `id` int(11) NOT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  `patient_id` int(11) DEFAULT NULL,
  `prescription_date` datetime DEFAULT NULL,
  `medication` varchar(255) DEFAULT NULL,
  `dosage` varchar(255) DEFAULT NULL,
  `instructions` text DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `prescriptions`
--

INSERT INTO `prescriptions` (`id`, `doctor_id`, `patient_id`, `prescription_date`, `medication`, `dosage`, `instructions`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2023-05-19 18:30:00', 'Medicine A', '10mg', 'Take once daily', '2023-05-20 20:42:21', '2023-05-20 20:42:21'),
(2, 2, 2, '2023-05-20 18:30:00', 'Medicine B', '5mg', 'Take twice daily', '2023-05-20 20:42:21', '2023-05-20 20:42:21'),
(3, 1, 3, '2023-05-21 18:30:00', 'Medicine C', '20mg', 'Take with food', '2023-05-20 20:42:21', '2023-05-20 20:42:21'),
(4, 3, 2, '2023-05-22 18:30:00', 'Medicine D', '15mg', 'Take before bedtime', '2023-05-20 20:42:21', '2023-05-20 20:42:21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appointments`
--
ALTER TABLE `appointments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `doctor_id` (`doctor_id`),
  ADD KEY `patient_id` (`patient_id`);

--
-- Indexes for table `doctors`
--
ALTER TABLE `doctors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patients`
--
ALTER TABLE `patients`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `prescriptions`
--
ALTER TABLE `prescriptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `doctor_id` (`doctor_id`),
  ADD KEY `patient_id` (`patient_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appointments`
--
ALTER TABLE `appointments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `doctors`
--
ALTER TABLE `doctors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `patients`
--
ALTER TABLE `patients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `prescriptions`
--
ALTER TABLE `prescriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `appointments`
--
ALTER TABLE `appointments`
  ADD CONSTRAINT `appointments_ibfk_1` FOREIGN KEY (`doctor_id`) REFERENCES `doctors` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `appointments_ibfk_2` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `prescriptions`
--
ALTER TABLE `prescriptions`
  ADD CONSTRAINT `prescriptions_ibfk_1` FOREIGN KEY (`doctor_id`) REFERENCES `doctors` (`id`),
  ADD CONSTRAINT `prescriptions_ibfk_2` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
