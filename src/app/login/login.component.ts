import { Component } from '@angular/core';
import { Router } from '@angular/router'; // Import the Router service
import Swal from 'sweetalert2'; // Importing swal for notifications
import { NgForm } from '@angular/forms'; // Import NgForm for template-driven forms

import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  formData = { username: '', password: '' };

  constructor(private authService: AuthService, private router: Router) {}

  performLogin(username: string, password: string): void {
    if (this.authService.login(username, password)) {
      // Successful login, set the authentication status
      this.authService.setAuthenticated(true);
      // Optionally, navigate to another route
      // this.router.navigate(['/dashboard']);
    } else {
      // Handle failed login
    }
  }
  onSubmit(form: NgForm): void {
    const { username, password } = this.formData;

    if (this.authService.login(username, password)) {
      // Successful login logic
      this.authService.setAuthenticated(true);
      // Redirect to the authenticated route
      this.router.navigate(['/dashboard']);
    } else {
      // Failed login logic
      Swal.fire({
      icon: 'error',
      title: 'Incorrect Password',
      text: 'The password you entered is incorrect. Please try again.',
    });
    }
  }

}
